# Generated by Django 2.2.11 on 2020-04-01 14:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20200401_1334'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='comment',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
