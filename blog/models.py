from django.conf import settings
from django.db import models
from django.utils import timezone


class Post(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    video = models.ManyToManyField("Video")

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        n = len(self.les_videos())
        return "Article : « "+ self.title + " » ("+ str(n) +" vidéos)"

    def les_videos(self):
        return self.video.all()

class Video(models.Model):
    url = models.CharField(max_length=200)
    comment = models.CharField(max_length=200)
    
    def __str__(self):
        return self.comment
    
    
